from django.conf import settings
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny

import requests

from .serializers import CreateUserSerializer


CLIENT_ID = settings.APP_CLIENT_ID
CLIENT_SECRET = settings.APP_CLIENT_SECRET



@api_view(['POST'])
@permission_classes([AllowAny])
def register(request):
    '''
    Registers user to the server. Input format:
    {"email": "email@example.com", "password": "1234abcd"}
    '''
    # Create serializer from data request
    serializer = CreateUserSerializer(data=request.data) 
    
    if serializer.is_valid():
        # If valid, save it and creates user
        serializer.save() 
        
        # Get token for the user.
        r = requests.post('http://localhost:8000/o/token/', 
            data={
                'grant_type': 'password',
                'email': request.data['email'],
                'password': request.data['password'],
                'username': request.data['email'],
                'client_id': CLIENT_ID,
                'client_secret': CLIENT_SECRET,
            },
        )
        return Response(r.json())
    return Response(serializer.errors)



@api_view(['POST'])
@permission_classes([AllowAny])
def token(request):
    '''
    Gets tokens with email and password. Input format:
    {"email": "email@example.com", "password": "1234abcd"}
    '''
    print("test token call")
    r = requests.post(
    'http://localhost:8000/o/token/', 
        data={
            'grant_type': 'password',
            'email': request.data['email'],
            'password': request.data['password'],
            'username': request.data['email'],
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
        },
    )
    
    return Response(r.json())



@api_view(['POST'])
@permission_classes([AllowAny])
def refresh_token(request):
    '''
    Registers user to the server. Input format:
    {"refresh_token": "<token>"}
    '''
    r = requests.post(
    'http://localhost:8000/o/token/', 
        data={
            'grant_type': 'refresh_token',
            'refresh_token': request.data['refresh_token'],
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
        },
    )
    return Response(r.json())


@api_view(['POST'])
@permission_classes([AllowAny])
def revoke_token(request):
    '''
    Method to revoke tokens.
    {"token": "<token>"}
    '''
    r = requests.post(
        'http://localhost:8000/o/revoke_token/', 
        data={
            'token': request.data['token'],
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
        },
    )
    # return success message
    if r.status_code == requests.codes.ok:
        return Response({'message': 'token revoked'}, r.status_code)
    
    # return any error
    return Response(r.json(), r.status_code)